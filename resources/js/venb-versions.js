export const versions = {
    v2018aj: {
        description: "Aanslagjaar 2018",
        name: 'v2018aj',
        component: () => import('./components/Venb2018.vue')
    },
    v2019ajOld: {
        description: "Aanslagjaar 2019 - oud",
        title: "Het belastbaar tijdperk vangt vóór 01.01.2018 aan.",
        name: 'v2019ajOld',
        component: () => import('./components/Venb2019Old.vue')
    },
    v2019ajNew: {
        description: "Aanslagjaar 2019 - nieuw",
        name: 'v2019ajNew',
        title: "Het belastbaar tijdperk vangt ten vroegste aan op 01.01.2018.",
        component: () => import('./components/Venb2019New.vue')
    },
    v2020aj : {
        description :  "Aanslagjaar 2020",
        name :"v2020aj",
        component: () => import('./components/Venb2020.vue')
    },
    v2021ajOld :{
        description :  "Aanslagjaar 2021 - Oud",
        name :"v2021ajOld",
        title: "Het belastbaar tijdperk vangt vóór 01.01.2020 aan",
        component: () => import('./components/Venb2021Oud.vue')
    },

    v2021ajNew :{
        description :  "Aanslagjaar 2021 - Nieuw",
        name :"v2021ajNew",
        title: "Het belastbaar tijdperk vangt ten vroegste aan op 01.01.2020",
        component: () => import('./components/Venb2021New.vue')
    },

    v2022aj :{
        description :  "Aanslagjaar 2022",
        name :"v2022aj",
        component: () => import('./components/Venb2022.vue')
    }
};

/**
 * Returns the latest version.
 * @returns {number}
 */
export function latestVersion() {
    return 'v2021ajNew'
}

/**
 * Registers the venb version components to the given Vue instance.
 * @param Vue
 */
export default function registerVenbVersions(Vue) {
    for (let [key, value] of Object.entries(versions)) {
        Vue.component(key, value.component)
    }
}
