/* Gegevens komen uit submodules/venb/resources/js/components
   (overeenstemmende Venb2022.vue component)
   Validation: app/src/Venb/resources/js/VenbCaculatorFactory.php */

export default class Venb2022Databag{

    /*Algemeen*/
    version = "v2022aj";
    isAanspraakVerminderdTarief = false;
    isVenootshapHuisvesting = false;

    /*Gewone aanslagen*/
    belastbaarGewoonTarief;
    winstTaxShelter3399;
    winstTaxShelter2958;
    winstTaxShelter25;
    verwezenlijkteMeerwaarde3399;
    investeringsReserve3399;
    verwezenlijkteMeerwaarde2958;
    investeringsReserve2958;
    belastbaarExitTaks15;
    kapitaalenInterestsubsidiesLandbouw05;
    mobilisatieReserves15;
    mobilisatieReserves10;

    /*Afzonderlijke aanslag COVID-19-pandemie*/
    afzonderlijkeAanslagCovid;
    tariefAfzonderlijkeAanslagCovid;

    /*Afzonderlijke aanslagen*/
    afzonderlijkeAanslagAlleAard50;
    afzonderlijkeAanslagAlleAard100;
    afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    afzonderlijkeAanslagUitgekeerdeDividenden;
    afzonderlijkeAanslagLiqRes10;

    /*Bijzondere aanslagen*/
    geheleOfGedeeltelijkMaatschappelijkVermogen33;
    geheleOfGedeeltelijkMaatschappelijkVermogen165;
    voordeleAlleAardVerleend;

    /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
    terugbetalingTegen100;

    /*Grondslag van de exitheffing*/
    latenteMeerwaarden;

    /*Verrekenbare voorheffingen*/
    nietterugbetaalbareVoorheffing;
    buitenlandseBelastingkrediet;
    terugbetaalbareVoorheffing;
    belastingkredietOndOntw;

    /*Voorafbetalingen*/
    isStartendKMO= false;
    isVerhoogdePercentagesVA3VA4;
    aantalkwartalen = 4;
    voorafbetalingKwart1;
    voorafbetalingKwart2;
    voorafbetalingKwart3;
    voorafbetalingKwart4;

    /*Correcties*/
    /*niet meer van toepassing vanaf AJ2022*/

    static copy(input){
        let tbr = new Venb2022Databag();

        /*Algemeen*/
        tbr.version = input.version;
        tbr.isAanspraakVerminderdTarief = input.isAanspraakVerminderdTarief;
        tbr.isVenootshapHuisvesting = input.isVenootshapHuisvesting;

        /*Gewone aanslagen*/
        tbr.belastbaarGewoonTarief = input.belastbaarGewoonTarief;
        tbr.winstTaxShelter3399 = input.winstTaxShelter3399;
        tbr.winstTaxShelter2958 = input.winstTaxShelter2958;
        tbr.winstTaxShelter25 = input.winstTaxShelter25;
        tbr.verwezenlijkteMeerwaarde3399 = input.verwezenlijkteMeerwaarde3399;
        tbr.investeringsReserve3399 = input.investeringsReserve3399;
        tbr.verwezenlijkteMeerwaarde2958 = input.verwezenlijkteMeerwaarde2958;
        tbr.investeringsReserve2958 = input.investeringsReserve2958;
        tbr.belastbaarExitTaks15 = input.belastbaarExitTaks15;
        tbr.kapitaalenInterestsubsidiesLandbouw05 = input.kapitaalenInterestsubsidiesLandbouw05;
        tbr.mobilisatieReserves15 = input.mobilisatieReserves10;
        tbr.mobilisatieReserves10 = input.mobilisatieReserves15;

        /*Afzonderlijke aanslag COVID-19-pandemie*/
        tbr.afzonderlijkeAanslagCovid = input.afzonderlijkeAanslagCovid;
        tbr.tariefAfzonderlijkeAanslagCovid = input.tariefAfzonderlijkeAanslagCovid;

        /*Afzonderlijke aanslagen*/
        tbr.afzonderlijkeAanslagAlleAard50 = input.afzonderlijkeAanslagAlleAard50;
        tbr.afzonderlijkeAanslagAlleAard100 = input.afzonderlijkeAanslagAlleAard100;
        tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
        tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
        tbr.afzonderlijkeAanslagUitgekeerdeDividenden = input.afzonderlijkeAanslagUitgekeerdeDividenden;
        tbr.afzonderlijkeAanslagLiqRes10 = input.afzonderlijkeAanslagLiqRes10;

        /*Bijzondere aanslagen*/
        tbr.geheleOfGedeeltelijkMaatschappelijkVermogen33 = input.geheleOfGedeeltelijkMaatschappelijkVermogen33;
        tbr.geheleOfGedeeltelijkMaatschappelijkVermogen165 = input.geheleOfGedeeltelijkMaatschappelijkVermogen165;
        tbr.voordeleAlleAardVerleend = input.voordeleAlleAardVerleend;

        /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
        tbr.terugbetalingTegen100 = input.terugbetalingTegen100;

        /*Grondslag van de exitheffing*/
        tbr.latenteMeerwaarden = input.latenteMeerwaarden;

        /*Verrekenbare voorheffingen*/
        tbr.nietterugbetaalbareVoorheffing = input.nietterugbetaalbareVoorheffing;
        tbr.buitenlandseBelastingkrediet = input.buitenlandseBelastingkrediet;
        tbr.terugbetaalbareVoorheffing = input.terugbetaalbareVoorheffing;
        tbr.belastingkredietOndOntw = input.belastingkredietOndOntw;

        /*Voorafbetalingen*/
        tbr.isStartendKMO = input.isStartendKMO;
        tbr.isVerhoogdePercentagesVA3VA4 = input.isVerhoogdePercentagesVA3VA4;
        tbr.aantalkwartalen = input.aantalkwartalen;
        tbr.voorafbetalingKwart1 = input.voorafbetalingKwart1;
        tbr.voorafbetalingKwart2 = input.voorafbetalingKwart2;
        tbr.voorafbetalingKwart3 = input.voorafbetalingKwart3;
        tbr.voorafbetalingKwart4 = input.voorafbetalingKwart4;

        /*Correcties*/
        /*niet meer van toepassing vanaf AJ2022*/

        return tbr;
    }

    equals(input){

            /*Algemeen*/
            return this.version === input.version &&
            this.isAanspraakVerminderdTarief === input.isAanspraakVerminderdTarief &&
            this.isVenootshapHuisvesting === input.isVenootshapHuisvesting &&

            /*Gewone aanslagen*/
            this.belastbaarGewoonTarief === input.belastbaarGewoonTarief &&
            this.winstTaxShelter3399 === input.winstTaxShelter3399 &&
            this.winstTaxShelter2958 === input.winstTaxShelter2958 &&
            this.winstTaxShelter25 === input.winstTaxShelter25 &&
            this.verwezenlijkteMeerwaarde3399 === input.verwezenlijkteMeerwaarde3399 &&
            this.investeringsReserve3399 === input.investeringsReserve3399 &&
            this.verwezenlijkteMeerwaarde2958 === input.verwezenlijkteMeerwaarde2958 &&
            this.investeringsReserve2958 === input.investeringsReserve2958 &&
            this.belastbaarExitTaks15 === input.belastbaarExitTaks15 &&
            this.kapitaalenInterestsubsidiesLandbouw05 === input.kapitaalenInterestsubsidiesLandbouw05 &&
            this.mobilisatieReserves15 === input.mobilisatieReserves15 &&
            this.mobilisatieReserves10 === input.mobilisatieReserves10 &&

            /*Afzonderlijke aanslag COVID-19-pandemie*/
            this.afzonderlijkeAanslagCovid === input.afzonderlijkeAanslagCovid &&
            this.tariefAfzonderlijkeAanslagCovid === input.tariefAfzonderlijkeAanslagCovid &&

            /*Afzonderlijke aanslagen*/
            this.afzonderlijkeAanslagAlleAard50 === input.afzonderlijkeAanslagAlleAard50 &&
            this.afzonderlijkeAanslagAlleAard100 === input.afzonderlijkeAanslagAlleAard100 &&
            this.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 &&
            this.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 &&
            this.afzonderlijkeAanslagUitgekeerdeDividenden === input.afzonderlijkeAanslagUitgekeerdeDividenden &&
            this.afzonderlijkeAanslagLiqRes10 === input.afzonderlijkeAanslagLiqRes10 &&

            /*Bijzondere aanslagen*/
            this.geheleOfGedeeltelijkMaatschappelijkVermogen33 === input.geheleOfGedeeltelijkMaatschappelijkVermogen33 &&
            this.geheleOfGedeeltelijkMaatschappelijkVermogen165 === input.geheleOfGedeeltelijkMaatschappelijkVermogen165 &&
            this.voordeleAlleAardVerleend === input.voordeleAlleAardVerleend &&

            /*Terugbetaling belastingkrediet voor onderzoek en ontwikkkeling*/
            this.terugbetalingTegen100 === input.terugbetalingTegen100 &&

            /*Grondslag van de exitheffing*/
            this.latenteMeerwaarden === input.latenteMeerwaarden &&

            /*Verrekenbare voorheffingen*/
            this.nietterugbetaalbareVoorheffing === input.nietterugbetaalbareVoorheffing &&
            this.buitenlandseBelastingkrediet === input.buitenlandseBelastingkrediet &&
            this.terugbetaalbareVoorheffing === input.terugbetaalbareVoorheffing &&
            this.belastingkredietOndOntw === input.belastingkredietOndOntw &&

            /*Voorafbetalingen*/
            this.isStartendKMO === input.isStartendKMO &&
            this.isVerhoogdePercentagesVA3VA4 === input.isVerhoogdePercentagesVA3VA4 &&
            this.aantalkwartalen === input.aantalkwartalen &&
            this.voorafbetalingKwart1 === input.voorafbetalingKwart1 &&
            this.voorafbetalingKwart2 === input.voorafbetalingKwart2 &&
            this.voorafbetalingKwart3 === input.voorafbetalingKwart3 &&
            this.voorafbetalingKwart4 === input.voorafbetalingKwart4

            /*Correcties*/
            /*niet meer van toepassing vanaf AJ2022*/

    }

}