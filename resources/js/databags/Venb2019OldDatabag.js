/* Gegevens komen uit submodules/venb/resources/js/components
   (overeenstemmende Venb2019Old.vue component)
   Validation: app/src/Venb/resources/js/VenbCaculatorFactory.php */

export default class Venb2019OldDatabag{

     /*Algemeen*/
     version = "v2019ajOld";
     isAanspraakVerminderdTarief = false;
     isVenootshapHuisvesting = false;
     isWijzigingAfsluitdatum26072017;

     /*Gewone aanslagen*/
     belastbaarGewoonTarief;
     meerwaardeOpAandelen25;
     belastbareTegenExitTaks165;
     belastbareTegenExitTaks125;
     meerwaardeOpAAndelenBelastbaartegen04;
     kapitaalenInterestsubsidiesLandbouw05;

     /*Afzonderlijke aanslagen*/
     afzonderlijkeAanslagAlleAard50;
     afzonderlijkeAanslagAlleAard100;
     afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
     afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
     afzonderlijkeAanslagUitgekeerdeDividenden;
     afzonderlijkeAanslagLiqRes10;

     /*Bijzondere aanslagen*/
     geheleOfGedeeltelijkMaatschappelijkVermogen33;
     geheleOfGedeeltelijkMaatschappelijkVermogen165;
     voordeleAlleAardVerleend;

     /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
     terugbetalingTegen100;

     /*Grondslag van de exitheffing*/
     latenteMeerwaarden;

     /*Verrekenbare voorheffingen*/
     nietterugbetaalbareVoorheffing;
     buitenlandseBelastingkrediet;
     terugbetaalbareVoorheffing;
     belastingkredietOndOntw;

     /*Voorafbetalingen*/
     isStartendKMO = false;
     aantalkwartalen = 4;
     voorafbetalingKwart1;
     voorafbetalingKwart2;
     voorafbetalingKwart3;
     voorafbetalingKwart4;

     /*Correcties*/
     negatieveCorrectieGrondslagGewoonTarief;
     positieveCorrectieGrondslagGewoonTarief;
     negatieveCorrectieMeerwaardeAandelen25;
     negatieveCorrectieMeerwaardeAandelen04;
     gedeelteMeerwaardeBelastbaar25;
     gedeelteGrondslagExitTaks125;
     gedeelteGecorrigeerdeAanslag29;
     isCorrectieVerlaagdTarief;
     
     static copy(input){
          let tbr = new Venb2019OldDatabag();

          /*Algemeen*/
          tbr.version = input.version;
          tbr.isAanspraakVerminderdTarief =  input.isAanspraakVerminderdTarief ;
          tbr.isVenootshapHuisvesting = input.isVenootshapHuisvesting ;
          tbr.isWijzigingAfsluitdatum26072017 = input.isWijzigingAfsluitdatum26072017 ;

          /*Gewone aanslagen*/
          tbr.belastbaarGewoonTarief = input.belastbaarGewoonTarief ;
          tbr.meerwaardeOpAandelen25 = input.meerwaardeOpAandelen25 ;
          tbr.belastbareTegenExitTaks165 = input.belastbareTegenExitTaks165 ;
          tbr.belastbareTegenExitTaks125 = input.belastbareTegenExitTaks125 ;
          tbr.meerwaardeOpAAndelenBelastbaartegen04 = input.meerwaardeOpAAndelenBelastbaartegen04 ;
          tbr.kapitaalenInterestsubsidiesLandbouw05 = input.kapitaalenInterestsubsidiesLandbouw05 ;

          /*Afzonderlijke aanslagen*/
          tbr.afzonderlijkeAanslagAlleAard50 = input.afzonderlijkeAanslagAlleAard50 ;
          tbr.afzonderlijkeAanslagAlleAard100 = input.afzonderlijkeAanslagAlleAard100 ;
          tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 ;
          tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 ;
          tbr.afzonderlijkeAanslagUitgekeerdeDividenden = input.afzonderlijkeAanslagUitgekeerdeDividenden ;
          tbr.afzonderlijkeAanslagLiqRes10 = input.afzonderlijkeAanslagLiqRes10 ;

          /*Bijzondere aanslagen*/
          tbr.geheleOfGedeeltelijkMaatschappelijkVermogen33 = input.geheleOfGedeeltelijkMaatschappelijkVermogen33 ;
          tbr.geheleOfGedeeltelijkMaatschappelijkVermogen165 = input.geheleOfGedeeltelijkMaatschappelijkVermogen165 ;
          tbr.voordeleAlleAardVerleend = input.voordeleAlleAardVerleend ;

          /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
          tbr.terugbetalingTegen100 = input.terugbetalingTegen100 ;

          /*Grondslag van de exitheffing*/
          tbr.latenteMeerwaarden = input.latenteMeerwaarden ;

          /*Verrekenbare voorheffingen*/
          tbr.nietterugbetaalbareVoorheffing = input.nietterugbetaalbareVoorheffing ;
          tbr.buitenlandseBelastingkrediet = input.buitenlandseBelastingkrediet ;
          tbr.terugbetaalbareVoorheffing = input.terugbetaalbareVoorheffing ;
          tbr.belastingkredietOndOntw = input.belastingkredietOndOntw ;

          /*Voorafbetalingen*/
          tbr.isStartendKMO = input.isStartendKMO ;
          tbr.voorafbetalingKwart1 = input.voorafbetalingKwart1 ;
          tbr.voorafbetalingKwart2 = input.voorafbetalingKwart2 ;
          tbr.voorafbetalingKwart3 = input.voorafbetalingKwart3 ;
          tbr.voorafbetalingKwart4 = input.voorafbetalingKwart4 ;
          tbr.aantalkwartalen = input.aantalkwartalen ;

          /*Correcties*/
          tbr.negatieveCorrectieGrondslagGewoonTarief = input.negatieveCorrectieGrondslagGewoonTarief ;
          tbr.positieveCorrectieGrondslagGewoonTarief = input.positieveCorrectieGrondslagGewoonTarief ;
          tbr.negatieveCorrectieMeerwaardeAandelen25 = input.negatieveCorrectieMeerwaardeAandelen25 ;
          tbr.negatieveCorrectieMeerwaardeAandelen04 = input.negatieveCorrectieMeerwaardeAandelen04 ;
          tbr.gedeelteMeerwaardeBelastbaar25 = input.gedeelteMeerwaardeBelastbaar25 ;
          tbr.gedeelteGrondslagExitTaks125 = input.gedeelteGrondslagExitTaks125 ;
          tbr.gedeelteGecorrigeerdeAanslag29 = input.gedeelteGecorrigeerdeAanslag29 ;
          tbr.isCorrectieVerlaagdTarief = input.isCorrectieVerlaagdTarief ;

          return tbr;
     }
     
     equals(input){
          /*Algemeen*/
          return this.version === input.version &&
          this.isAanspraakVerminderdTarief ===  input.isAanspraakVerminderdTarief &&
          this.isVenootshapHuisvesting === input.isVenootshapHuisvesting &&
          this.isWijzigingAfsluitdatum26072017 === input.isWijzigingAfsluitdatum26072017 &&

          /*Gewone aanslagen*/
          this.belastbaarGewoonTarief === input.belastbaarGewoonTarief &&
          this.meerwaardeOpAandelen25 === input.meerwaardeOpAandelen25 &&
          this.belastbareTegenExitTaks165 === input.belastbareTegenExitTaks165 &&
          this.belastbareTegenExitTaks125 === input.belastbareTegenExitTaks125 &&
          this.meerwaardeOpAAndelenBelastbaartegen04 === input.meerwaardeOpAAndelenBelastbaartegen04 &&
          this.kapitaalenInterestsubsidiesLandbouw05 === input.kapitaalenInterestsubsidiesLandbouw05 &&

          /*Afzonderlijke aanslagen*/
          this.afzonderlijkeAanslagAlleAard50 === input.afzonderlijkeAanslagAlleAard50 &&
          this.afzonderlijkeAanslagAlleAard100 === input.afzonderlijkeAanslagAlleAard100 &&
          this.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 &&
          this.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 &&
          this.afzonderlijkeAanslagUitgekeerdeDividenden === input.afzonderlijkeAanslagUitgekeerdeDividenden &&
          this.afzonderlijkeAanslagLiqRes10 === input.afzonderlijkeAanslagLiqRes10 &&

          /*Bijzondere aanslagen*/
          this.geheleOfGedeeltelijkMaatschappelijkVermogen33 === input.geheleOfGedeeltelijkMaatschappelijkVermogen33 &&
          this.geheleOfGedeeltelijkMaatschappelijkVermogen165 === input.geheleOfGedeeltelijkMaatschappelijkVermogen165 &&
          this.voordeleAlleAardVerleend === input.voordeleAlleAardVerleend &&

          /*Terugbetling belastingkrediet onderzoek en ontwikkeling*/
          this.terugbetalingTegen100 === input.terugbetalingTegen100 &&

          /*Grondslag van de exitheffing*/
          this.latenteMeerwaarden === input.latenteMeerwaarden &&

          /*Verrekenbare voorheffingen*/
          this.nietterugbetaalbareVoorheffing === input.nietterugbetaalbareVoorheffing &&
          this.buitenlandseBelastingkrediet === input.buitenlandseBelastingkrediet &&
          this.terugbetaalbareVoorheffing === input.terugbetaalbareVoorheffing &&
          this.belastingkredietOndOntw === input.belastingkredietOndOntw &&

          /*Voorafbetalingen*/
          this.isStartendKMO === input.isStartendKMO &&
          this.aantalkwartalen === input.aantalkwartalen &&
          this.voorafbetalingKwart1 === input.voorafbetalingKwart1 &&
          this.voorafbetalingKwart2 === input.voorafbetalingKwart2 &&
          this.voorafbetalingKwart3 === input.voorafbetalingKwart3 &&
          this.voorafbetalingKwart4 === input.voorafbetalingKwart4 &&

          /*Correcties*/
          this.negatieveCorrectieGrondslagGewoonTarief === input.negatieveCorrectieGrondslagGewoonTarief &&
          this.positieveCorrectieGrondslagGewoonTarief === input.positieveCorrectieGrondslagGewoonTarief &&
          this.negatieveCorrectieMeerwaardeAandelen25 === input.negatieveCorrectieMeerwaardeAandelen25 &&
          this.negatieveCorrectieMeerwaardeAandelen04 === input.negatieveCorrectieMeerwaardeAandelen04 &&
          this.gedeelteMeerwaardeBelastbaar25 === input.gedeelteMeerwaardeBelastbaar25 &&
          this.gedeelteGrondslagExitTaks125 === input.gedeelteGrondslagExitTaks125 &&
          this.gedeelteGecorrigeerdeAanslag29 === input.gedeelteGecorrigeerdeAanslag29 &&
          this.isCorrectieVerlaagdTarief === input.isCorrectieVerlaagdTarief
     }
}