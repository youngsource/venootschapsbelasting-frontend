/* Gegevens komen uit submodules/venb/resources/js/components
   (overeenstemmende Venb2021Old.vue component)
   Validation: app/src/Venb/resources/js/VenbCaculatorFactory.php */

export default class Venb2021OldDatabag{

    /*Algemeen*/
    version = "v2021ajOld";
    isAanspraakVerminderdTarief = false;
    isVenootshapHuisvesting = false;
    isWijzigingAfsluitdatum26072017;

    /*Gewone aanslagen*/
    belastbaarGewoonTarief;
    meerwaardeOpAandelen25;
    winstTaxShelter3399;
    winstTaxShelter2958;
    winstTaxShelter25;
    verwezenlijkteMeerwaarde3399;
    investeringsReserve3399;
    verwezenlijkteMeerwaarde2958;
    investeringsReserve2958;
    belastbareTegenExitTaks125;
    belastbaarExitTaks15;
    kapitaalenInterestsubsidiesLandbouw05;
    mobilisatieReserves15;
    mobilisatieReserves10;

    /*Afzonderlijke aanslag COVID-19-pandemie*/
    afzonderlijkeAanslagCovid;
    tariefAfzonderlijkeAanslagCovid;

    /*Afzonderlijke aanslagen*/
    afzonderlijkeAanslagAlleAard50;
    afzonderlijkeAanslagAlleAard100;
    afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    afzonderlijkeAanslagUitgekeerdeDividenden;
    afzonderlijkeAanslagLiqRes10;

    /*Bijzondere aanslagen*/
    geheleOfGedeeltelijkMaatschappelijkVermogen33;
    geheleOfGedeeltelijkMaatschappelijkVermogen165;
    voordeleAlleAardVerleend;

    /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
    terugbetalingTegen100;

    /*Grondslag van de exitheffing*/
    latenteMeerwaarden;

    /*Verrekenbare voorheffingen*/
    nietterugbetaalbareVoorheffing;
    buitenlandseBelastingkrediet;
    terugbetaalbareVoorheffing;
    belastingkredietOndOntw;

    /*Voorafbetalingen*/
    isStartendKMO= false;
    isVerhoogdePercentagesVA3VA4;
    aantalkwartalen = 4;
    voorafbetalingKwart1;
    voorafbetalingKwart2;
    voorafbetalingKwart3;
    voorafbetalingKwart4;

    /*Correcties*/
    gedeelteGrondslagBelastbaar25;
    gedeelteGrondslagExitTarief15ZonderACB;

    static copy(input){
        let tbr = new Venb2021OldDatabag();

        /*Algemeen*/
        tbr.version = input.version;
        tbr.isAanspraakVerminderdTarief = input.isAanspraakVerminderdTarief;
        tbr.isVenootshapHuisvesting = input.isVenootshapHuisvesting;
        tbr.isWijzigingAfsluitdatum26072017 = input.isWijzigingAfsluitdatum26072017;

        /*Gewone aanslagen*/
        tbr.belastbaarGewoonTarief = input.belastbaarGewoonTarief;
        tbr.meerwaardeOpAandelen25 = input.meerwaardeOpAandelen25;
        tbr.winstTaxShelter3399 = input.winstTaxShelter3399;
        tbr.winstTaxShelter2958 = input.winstTaxShelter2958;
        tbr.winstTaxShelter25 = input.winstTaxShelter25;
        tbr.verwezenlijkteMeerwaarde3399 = input.verwezenlijkteMeerwaarde3399;
        tbr.investeringsReserve3399 = input.investeringsReserve3399;
        tbr.verwezenlijkteMeerwaarde2958 = input.verwezenlijkteMeerwaarde2958;
        tbr.investeringsReserve2958 = input.investeringsReserve2958;
        tbr.belastbareTegenExitTaks125 = input.belastbareTegenExitTaks125;
        tbr.belastbaarExitTaks15 = input.belastbaarExitTaks15;
        tbr.kapitaalenInterestsubsidiesLandbouw05 = input.kapitaalenInterestsubsidiesLandbouw05;
        tbr.mobilisatieReserves15 = input.mobilisatieReserves15;
        tbr.mobilisatieReserves10 = input.mobilisatieReserves10;

        /*Afzonderlijke aanslag COVID-19-pandemie*/
        tbr.afzonderlijkeAanslagCovid = input.afzonderlijkeAanslagCovid;
        tbr.tariefAfzonderlijkeAanslagCovid = input.tariefAfzonderlijkeAanslagCovid;

        /*Afzonderlijke aanslagen*/
        tbr.afzonderlijkeAanslagAlleAard50 = input.afzonderlijkeAanslagAlleAard50;
        tbr.afzonderlijkeAanslagAlleAard100 = input.afzonderlijkeAanslagAlleAard100;
        tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
        tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
        tbr.afzonderlijkeAanslagUitgekeerdeDividenden = input.afzonderlijkeAanslagUitgekeerdeDividenden;
        tbr.afzonderlijkeAanslagLiqRes10 = input.afzonderlijkeAanslagLiqRes10;

        /*Bijzondere aanslagen*/
        tbr.geheleOfGedeeltelijkMaatschappelijkVermogen33 = input.geheleOfGedeeltelijkMaatschappelijkVermogen33;
        tbr.geheleOfGedeeltelijkMaatschappelijkVermogen165 = input.geheleOfGedeeltelijkMaatschappelijkVermogen165;
        tbr.voordeleAlleAardVerleend = input.voordeleAlleAardVerleend;

        /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
        tbr.terugbetalingTegen100 = input.terugbetalingTegen100;

        /*Grondslag van de exitheffing*/
        tbr.latenteMeerwaarden = input.latenteMeerwaarden;

        /*Verrekenbare voorheffingen*/
        tbr.nietterugbetaalbareVoorheffing = input.nietterugbetaalbareVoorheffing;
        tbr.buitenlandseBelastingkrediet = input.buitenlandseBelastingkrediet;
        tbr.terugbetaalbareVoorheffing = input.terugbetaalbareVoorheffing;
        tbr.belastingkredietOndOntw = input.belastingkredietOndOntw;

        /*Voorafbetalingen*/
        tbr.isStartendKMO = input.isStartendKMO;
        tbr.isVerhoogdePercentagesVA3VA4 = input.isVerhoogdePercentagesVA3VA4;
        tbr.aantalkwartalen = input.aantalkwartalen;
        tbr.voorafbetalingKwart1 = input.voorafbetalingKwart1;
        tbr.voorafbetalingKwart2 = input.voorafbetalingKwart2;
        tbr.voorafbetalingKwart3 = input.voorafbetalingKwart3;
        tbr.voorafbetalingKwart4 = input.voorafbetalingKwart4;

        /*Correcties*/
        tbr.gedeelteGrondslagBelastbaar25 = input.gedeelteGrondslagBelastbaar25;
        tbr.gedeelteGrondslagExitTarief15ZonderACB = input.gedeelteGrondslagExitTarief15ZonderACB;

        return tbr;
    }

    equals(input){

        /*Algemeen*/
        return this.version === input.version &&
        this.isAanspraakVerminderdTarief === input.isAanspraakVerminderdTarief &&
        this.isVenootshapHuisvesting === input.isVenootshapHuisvesting &&
        this.isWijzigingAfsluitdatum26072017 === input.isWijzigingAfsluitdatum26072017 &&

        /*Gewone aanslagen*/
        this.belastbaarGewoonTarief === input.belastbaarGewoonTarief &&
        this.meerwaardeOpAandelen25 === input.meerwaardeOpAandelen25 &&
        this.winstTaxShelter3399 === input.winstTaxShelter3399 &&
        this.winstTaxShelter2958 === input.winstTaxShelter2958 &&
        this.winstTaxShelter25 === input.winstTaxShelter25 &&
        this.verwezenlijkteMeerwaarde3399 === input.verwezenlijkteMeerwaarde3399 &&
        this.investeringsReserve3399 === input.investeringsReserve3399 &&
        this.verwezenlijkteMeerwaarde2958 === input.verwezenlijkteMeerwaarde2958 &&
        this.investeringsReserve2958 === input.investeringsReserve2958 &&
        this.belastbareTegenExitTaks125 === input.belastbareTegenExitTaks125 &&
        this.belastbaarExitTaks15 === input.belastbaarExitTaks15 &&
        this.kapitaalenInterestsubsidiesLandbouw05 === input.kapitaalenInterestsubsidiesLandbouw05 &&
        this.mobilisatieReserves15 === input.mobilisatieReserves15 &&
        this.mobilisatieReserves10 === input.mobilisatieReserves10 &&

        /*Afzonderlijke aanslag COVID-19-pandemie*/
        this.afzonderlijkeAanslagCovid === input.afzonderlijkeAanslagCovid &&
        this.tariefAfzonderlijkeAanslagCovid === input.tariefAfzonderlijkeAanslagCovid &&

        /*Afzonderlijke aanslagen*/
        this.afzonderlijkeAanslagAlleAard50 === input.afzonderlijkeAanslagAlleAard50 &&
        this.afzonderlijkeAanslagAlleAard100 === input.afzonderlijkeAanslagAlleAard100 &&
        this.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 &&
        this.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 &&
        this.afzonderlijkeAanslagUitgekeerdeDividenden === input.afzonderlijkeAanslagUitgekeerdeDividenden &&
        this.afzonderlijkeAanslagLiqRes10 === input.afzonderlijkeAanslagLiqRes10 &&

        /*Bijzondere aanslagen*/
        this.geheleOfGedeeltelijkMaatschappelijkVermogen33 === input.geheleOfGedeeltelijkMaatschappelijkVermogen33 &&
        this.geheleOfGedeeltelijkMaatschappelijkVermogen165 === input.geheleOfGedeeltelijkMaatschappelijkVermogen165 &&
        this.voordeleAlleAardVerleend === input.voordeleAlleAardVerleend &&

        /*Terugbetaling belastingkrediet voor onderzoek en ontwikkkeling*/
        this.terugbetalingTegen100 === input.terugbetalingTegen100 &&

        /*Grondslag van de exitheffing*/
        this.latenteMeerwaarden === input.latenteMeerwaarden &&

        /*Verrekenbare voorheffingen*/
        this.nietterugbetaalbareVoorheffing === input.nietterugbetaalbareVoorheffing &&
        this.buitenlandseBelastingkrediet === input.buitenlandseBelastingkrediet &&
        this.terugbetaalbareVoorheffing === input.terugbetaalbareVoorheffing &&
        this.belastingkredietOndOntw === input.belastingkredietOndOntw &&

        /*Voorafbetalingen*/
        this.isStartendKMO === input.isStartendKMO &&
        this.isVerhoogdePercentagesVA3VA4 === input.isVerhoogdePercentagesVA3VA4 &&
        this.aantalkwartalen === input.aantalkwartalen &&
        this.voorafbetalingKwart1 === input.voorafbetalingKwart1 &&
        this.voorafbetalingKwart2 === input.voorafbetalingKwart2 &&
        this.voorafbetalingKwart3 === input.voorafbetalingKwart3 &&
        this.voorafbetalingKwart4 === input.voorafbetalingKwart4 &&

        /*Correcties*/
        this.gedeelteGrondslagBelastbaar25 === input.gedeelteGrondslagBelastbaar25 &&
        this.gedeelteGrondslagExitTarief15ZonderACB === input.gedeelteGrondslagExitTarief15ZonderACB


    }
     
}