/* Gegevens komen uit submodules/venb/resources/js/components
   (overeenstemmende Venb2018.vue component)
   Validation: app/src/Venb/resources/js/VenbCaculatorFactory.php */

export default class Venb2018InputDatabag{

    /*Algemeen*/
    version;
    isAanspraakVerminderdTarief = false;
    isVenootshapHuisvesting = false;

    /*Gewone aanslagen*/
    belastbaarGewoonTarief;
    meerwaardeOpAandelen25;
    belastbareTegenExitTaks165;
    belastbareTegenExitTaks125;
    meerwaardeOpAAndelenBelastbaartegen04;
    kapitaalenInterestsubsidiesLandbouw05;

    /*Afzonderlijke aanslagen*/
    afzonderlijkeAanslagAlleAard50;
    afzonderlijkeAanslagAlleAard100;
    afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    afzonderlijkeAanslagUitgekeerdeDividenden;
    fairnessTax;
    afzonderlijkeAanslagBoekhoudingNaWinst;

    /*Bijzondere aanslagen*/
    geheleOfGedeeltelijkMaatschappelijkVermogen33;
    geheleOfGedeeltelijkMaatschappelijkVermogen165;
    voordeleAlleAardVerleend;

    /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
    terugbetalingTegen100;

    /*Grondslag van de exitheffing*/
    latenteMeerwaarden;

    /*Verrekenbare voorheffingen*/
    nietterugbetaalbareVoorheffing;
    buitenlandseBelastingkrediet;
    terugbetaalbareVoorheffing;
    belastingkredietOndOntw;

    /*Voorafbetalingen*/
    isStartendKMO = false;
    aantalkwartalen = 4 ;
    voorafbetalingKwart1;
    voorafbetalingKwart2;
    voorafbetalingKwart3;
    voorafbetalingKwart4;

    static copy(input) {
        let tbr = new Venb2018InputDatabag;

        /*Algemeen*/
        tbr.version = input.version;
        tbr.isAanspraakVerminderdTarief = input.isAanspraakVerminderdTarief;
        tbr.isVenootshapHuisvesting = input.isVenootshapHuisvesting;

        /*Gewone aanslagen*/
        tbr.belastbaarGewoonTarief = input.belastbaarGewoonTarief;
        tbr.meerwaardeOpAandelen25 = input.meerwaardeOpAandelen25;
        tbr.belastbareTegenExitTaks165 = input.belastbareTegenExitTaks165;
        tbr.belastbareTegenExitTaks125 = input.belastbareTegenExitTaks125;
        tbr.meerwaardeOpAAndelenBelastbaartegen04 = input.meerwaardeOpAAndelenBelastbaartegen04;
        tbr.kapitaalenInterestsubsidiesLandbouw05 = input.kapitaalenInterestsubsidiesLandbouw05;

        /*Afzonderlijke aanslagen*/
        tbr.afzonderlijkeAanslagAlleAard50 = input.afzonderlijkeAanslagAlleAard50;
        tbr.afzonderlijkeAanslagAlleAard100 = input.afzonderlijkeAanslagAlleAard100;
        tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
        tbr.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 = input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
        tbr.afzonderlijkeAanslagUitgekeerdeDividenden = input.afzonderlijkeAanslagUitgekeerdeDividenden;
        tbr.fairnessTax = input.fairnessTax;
        tbr.afzonderlijkeAanslagBoekhoudingNaWinst = input.afzonderlijkeAanslagBoekhoudingNaWinst;

        /*Bijzondere aanslagen*/
        tbr.geheleOfGedeeltelijkMaatschappelijkVermogen33 = input.geheleOfGedeeltelijkMaatschappelijkVermogen33;
        tbr.geheleOfGedeeltelijkMaatschappelijkVermogen165 = input.geheleOfGedeeltelijkMaatschappelijkVermogen165;
        tbr.voordeleAlleAardVerleend = input.voordeleAlleAardVerleend;

        /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
        tbr.terugbetalingTegen100 = input.terugbetalingTegen100;

        /*Grondslag van de exitheffing*/
        tbr.latenteMeerwaarden = input.latenteMeerwaarden;

        /*Verrekenbare voorheffingen*/
        tbr.nietterugbetaalbareVoorheffing = input.nietterugbetaalbareVoorheffing ;
        tbr.buitenlandseBelastingkrediet = input.buitenlandseBelastingkrediet ;
        tbr.terugbetaalbareVoorheffing = input.terugbetaalbareVoorheffing ;
        tbr.belastingkredietOndOntw = input.belastingkredietOndOntw ;

        /*Voorafbetalingen*/
        tbr.isStartendKMO = input.isStartendKMO;
        tbr.aantalkwartalen = input.aantalkwartalen;
        tbr.voorafbetalingKwart1 = input.voorafbetalingKwart1 ;
        tbr.voorafbetalingKwart2 =  input.voorafbetalingKwart2;
        tbr.voorafbetalingKwart3 =  input.voorafbetalingKwart3;
        tbr.voorafbetalingKwart4 = input.voorafbetalingKwart4;


        return tbr;
    }

    equals(input){

        /*Algemeen*/
        return this.version === input.version &&
        this.isAanspraakVerminderdTarief === input.isAanspraakVerminderdTarief &&
        this.isVenootshapHuisvesting === input.isVenootshapHuisvesting &&

        /*Gewone aanslagen*/
        this.belastbaarGewoonTarief === input.belastbaarGewoonTarief &&
        this.meerwaardeOpAandelen25 === input.meerwaardeOpAandelen25 &&
        this.belastbareTegenExitTaks165 === input.belastbareTegenExitTaks165 &&
        this.belastbareTegenExitTaks125 === input.belastbareTegenExitTaks125 &&
        this.meerwaardeOpAAndelenBelastbaartegen04 === input.meerwaardeOpAAndelenBelastbaartegen04 &&
        this.kapitaalenInterestsubsidiesLandbouw05 === input.kapitaalenInterestsubsidiesLandbouw05 &&

        /*Afzonderlijke aanslagen*/
        this.afzonderlijkeAanslagAlleAard50 === input.afzonderlijkeAanslagAlleAard50 &&
        this.afzonderlijkeAanslagAlleAard100 === input.afzonderlijkeAanslagAlleAard100 &&
        this.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen34 &&
        this.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 === input.afzonderlijkeAanslagBelasteReservesKredietInstellingen28 &&
        this.afzonderlijkeAanslagUitgekeerdeDividenden === input.afzonderlijkeAanslagUitgekeerdeDividenden &&
        this.fairnessTax === input.fairnessTax &&
        this.afzonderlijkeAanslagBoekhoudingNaWinst === input.afzonderlijkeAanslagBoekhoudingNaWinst &&

        /*Bijzondere aanslagen*/
        this.geheleOfGedeeltelijkMaatschappelijkVermogen33 === input.geheleOfGedeeltelijkMaatschappelijkVermogen33 &&
        this.geheleOfGedeeltelijkMaatschappelijkVermogen165 === input.geheleOfGedeeltelijkMaatschappelijkVermogen165 &&
        this.voordeleAlleAardVerleend === input.voordeleAlleAardVerleend &&

        /*Terugbetaling belastingkrediet voor onderzoek en ontwikkeling*/
        this.terugbetalingTegen100 === input.terugbetalingTegen100 &&

        /*Grondslag van de exitheffing*/
        this.latenteMeerwaarden ===  input.latenteMeerwaarden &&

        /*Verrekenbare voorheffingen*/
        this.nietterugbetaalbareVoorheffing === input.nietterugbetaalbareVoorheffing &&
        this.buitenlandseBelastingkrediet === input.buitenlandseBelastingkrediet &&
        this.terugbetaalbareVoorheffing === input.terugbetaalbareVoorheffing &&
        this.belastingkredietOndOntw === input.belastingkredietOndOntw &&

        /*Voorafbetalingen*/
        this.isStartendKMO === input.isStartendKMO &&
        this.aantalkwartalen === input.aantalkwartalen &&
        this.voorafbetalingKwart1 === input.voorafbetalingKwart1 &&
        this.voorafbetalingKwart2 === input.voorafbetalingKwart2 &&
        this.voorafbetalingKwart3 === input.voorafbetalingKwart3 &&
        this.voorafbetalingKwart4 === input.voorafbetalingKwart4

    }
}